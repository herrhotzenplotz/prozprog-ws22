SUBDIR	= \
	aufgaben1 \
	aufgaben2 \
	aufgaben3 \
	aufgaben4 \
	aufgaben5 \
	aufgaben6 \
	aufgaben7

.PHONY: all clean ${SUBDIR} ${SUBDIR:=-clean}
all: ${SUBDIR}

${SUBDIR}:
	${MAKE} -C ${@} all

clean: ${SUBDIR:=-clean}

${SUBDIR:=-clean}:
	${MAKE} -C ${@:-clean=} clean
