#include <iostream>
#include <cstdlib>
#include <ctime>

class Roboter
{
public:
	Roboter(int id)
		: m_id(id)
		, m_fehlerstatus(false)
		, m_position(0.0)
	{
	}

	int get_id()
	{
		return m_id;
	}

	bool get_fehlerstatus()
	{
		return m_fehlerstatus;
	}

	double get_position()
	{
		return m_position;
	}

	void status()
	{
		std::cout << "Roboter {ID = " << m_id << ", status = ";

		if (m_fehlerstatus)
			std::cout << "funktioniert nicht, ";
		else
			std::cout << "funktioniert, ";

		std::cout << "position = " << m_position << " }\n";
	}

protected:
	int m_id;
	bool m_fehlerstatus;
	double m_position;

private:

};

class Laserscanner
{
public:
	Laserscanner()
		: m_abstand(0)
	{
	}

	double get_abstand()
	{
		return m_abstand;
	}

	void scan()
	{
		m_abstand = 25.0 * (rand()/double(RAND_MAX));
	}

private:
	double m_abstand;
};

class Husky : public Roboter
{
public:
	Husky(int id)
		: Roboter(id)
		, m_scanner()
	{
	}

	void fahren(double dist)
	{
		if (m_fehlerstatus)
			return;

		m_scanner.scan();

		if (m_scanner.get_abstand() < dist)
			m_fehlerstatus = true;
		else
			m_position += dist;
	}

private:
	Laserscanner m_scanner;
};

int main()
{
	Husky h1{42};

	srand(time(NULL));

	do {
		h1.fahren(0.5);
		h1.status();
	} while (!h1.get_fehlerstatus());

	return 0;
}
