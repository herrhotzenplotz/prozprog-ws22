#include <climits>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

int
main()
{
	int n; /* Anzahl der Zufallszahlen */
	int max_record = -INT_MAX, min_record = INT_MAX;
	double average = 0.0, stddev = 0.0;

	std::cout << "Bitte Anzahl der Zahlen eingeben: ";
	std::cin >> n;

	auto *numbers = new int[n];

	/* Zufallszahlengenerator initialisieren */
	srand((unsigned)time(NULL));

	/* Array mit 100 verschiedenen Zufallszahlen befüllen */
	for (int i = 0; i < n; ++i) {
		numbers[i] = rand() % 100;

		if (numbers[i] > max_record)
			max_record = numbers[i];

		if (numbers[i] < min_record)
			min_record = numbers[i];

		average += (double)numbers[i] / (double)n;
	}

	/* Standardabweichung berechnen */
	for (int i = 0; i < n; ++i) {
		stddev +=
			std::pow((double)numbers[i] - average, 2.0)
			/ ((double)n - 1.0);
	}

	std::cout
		<< "Maximum    = " << max_record << std::endl
		<< "Minimum    = " << min_record << std::endl
		<< "Mittelwert = " << average    << std::endl
		<< "Streuung   = " << stddev     << std::endl;


	delete[] numbers;
	return 0;
}
