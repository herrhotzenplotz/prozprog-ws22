#include <climits>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

int
main()
{
	/* Array mit 100 int-Werten */
	int numbers[100];
	/* Minimum- und Maximum-Rekord */
	int max_record = -INT_MAX, min_record = INT_MAX;
	/* Mittelwert und Streuung */
	double average = 0.0, stddev = 0.0;

	/* Zufallszahlengenerator initialisieren */
	srand((unsigned)time(NULL));

	/* Array mit 100 verschiedenen Zufallszahlen befüllen */
	for (int i = 0; i < 100; ++i) {
		numbers[i] = rand() % 100;

		if (numbers[i] > max_record)
			max_record = numbers[i];

		if (numbers[i] < min_record)
			min_record = numbers[i];

		average += (double)numbers[i] / 100.0;
	}

	/* Standardabweichung berechnen */
	for (int i = 0; i < 100; ++i) {
		stddev +=
			std::pow((double)numbers[i] - average, 2.0)
			/ (100.0 - 1.0);
	}

	std::cout
		<< "Maximum    = " << max_record << std::endl
		<< "Minimum    = " << min_record << std::endl
		<< "Mittelwert = " << average << std::endl
		<< "Streuung   = " << stddev << std::endl;


	return 0;
}
