#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

double
mittelwert(int numbers[], int numbers_size)
{
	double result = 0;

	for (int i = 0; i < numbers_size; ++i)
		result += (double)numbers[i] / (double)numbers_size;

	return result;
}

double
streuung(int numbers[], int numbers_size, double avrg)
{
	double result = 0.0;

	for (int i = 0; i < numbers_size; ++i)
		result += std::pow((double)numbers[i] - avrg, 2.0)
			/ (double)(numbers_size - 1);

	return result;
}

#define MAX 100

int
main()
{
	int nums[MAX] = {};

	srand(time(NULL));

	for (int i = 0; i < MAX; ++i) {
		nums[i] = rand() % 100;
	}

	double avrg = mittelwert(nums, MAX);
	double strg = streuung(nums, MAX, avrg);

	std::cout << "Mittelwert = " << avrg << "\n"
	          << "Streuung = " << strg << "\n";

	return 0;
}
