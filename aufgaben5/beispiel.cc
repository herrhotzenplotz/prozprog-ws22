#include <iostream>

void foo(int x, int& xref)
{
	std::cout << "(foo) x    = " << x << std::endl;
	std::cout << "(foo) xref = " << xref << std::endl;

	x = 42;
	xref = 42;

	std::cout << "(foo) x    = " << x << std::endl;
	std::cout << "(foo) xref = " << xref << std::endl;
}

int main()
{
	int y = 0, z = 0;

	std::cout << "(main) y = " << y << std::endl
	          << "(main) z = " << z << std::endl;

	foo(y, z);

	std::cout << "(main) y = " << y << std::endl
	          << "(main) z = " << z << std::endl;

	return 0;
}
