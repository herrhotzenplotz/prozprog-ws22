#include <iostream>
#include <cmath>

void
doit(double x, double y,              /* pass by value */
     int& quadrant, double& winkel)   /* pass by reference */
{
	winkel = atan2(y, x) * 180.0 / 3.141;
	winkel = fmod(winkel + 360.0, 360.0);

	if (x > 0 && y > 0)
		quadrant = 1;
	else if (x < 0 && y > 0)
		quadrant = 2;
	else if (x > 0 && y < 0)
		quadrant = 4;
	else if (x < 0 && y < 0)
		quadrant = 3;
}

int main()
{
	double x, y, a;
	int q;

	for (;;) {
		std::cout << "Bitte x und y eingeben: ";
		std::cin >> x >> y;

		if (x == 0 || y == 0)
			return 0;

		/*   v--- call by value */
		doit(x, y, q, a);
		/*         ^-- call by reference */

		if (q == 0)
			std::cout << "Quadrant ist nicht eindeutig" << std::endl;
		else
			std::cout << "Quadrant = " << q << std::endl;

		std::cout << "Winkel = " << a << std::endl;
	}

	return 0;
}
