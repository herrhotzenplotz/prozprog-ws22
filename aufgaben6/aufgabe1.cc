#include <iostream>

class bewertung
{
public:
	bewertung()
		: m_id(0)
		, m_geschmack(0)
		, m_aussehen(0)
	{
	}

	bewertung(int id, double geschmack, double aussehen)
		: m_id(id)
		, m_geschmack(geschmack)
		, m_aussehen(aussehen)
	{
	}

	void set_geschmack(double geschmack)
	{
		if (geschmack < 0 || geschmack > 5.0)
			return;

		m_geschmack = geschmack;
	}

	void set_aussehen(double aussehen)
	{
		m_aussehen = aussehen;
	}

	void set_id(int id)
	{
		m_id = id;
	}

	double get_geschmack()
	{
		return m_geschmack;
	}

	int get_id()
	{
		return m_id;
	}

	void ausgabe()
	{
		std::cout << "ID = " <<  m_id
		          << ", Aussehen = " << m_aussehen
		          << ", Geschmack = " << m_geschmack
		          << "\n";
	}

private:
	int m_id;
	double m_geschmack;
	double m_aussehen;
};

bewertung read_bewertung()
{
	int id;
	double geschmack;
	double aussehen;

	std::cout << "ID: ";
	std::cin >> id;

	std::cout << "Geschmack: ";
	std::cin >> geschmack;

	std::cout << "Aussehen: ";
	std::cin >> aussehen;

	return bewertung{id, geschmack, aussehen};
}

void print_bewertung_of_producer(bewertung *bewertungen, int n, int pid)
{
	for (int i = 0; i < n; ++i) {
		if (bewertungen[i].get_id() == pid)
			bewertungen[i].ausgabe();
	}
}

int main()
{
	bewertung *Bewertungen;
	int n = 0;

	std::cout << "Anzahl der Bewertungen: ";
	std::cin >> n;

	Bewertungen = new bewertung[n];

	for (int i = 0; i < n; ++i) {
		Bewertungen[i] = read_bewertung();
	}


	for (;;) {
		int produzenten_id;

		std::cout << "Produzenten ID: ";
		std::cin >> produzenten_id;

		if (produzenten_id < 0)
			break;

		print_bewertung_of_producer(Bewertungen, n, produzenten_id);
	}


	delete[] Bewertungen;

	return 0;
}
