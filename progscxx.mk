CXX	=	c++
CXXFLAGS=	-g -O0 -std=c++17 -Wall -Wextra -Werror
LDFLAGS=	-lm -lrt

PROGS	?=	${PROG}

.PHONY: all clean
all: ${PROGS}

.SUFFIXES: .cc

"": .cc
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o ${PROG} $<

clean:
	rm -f ${PROGS} ${OBJS}
