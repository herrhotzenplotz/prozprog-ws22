# Mitschriften Prozedurale Programmierung WS 2022

Dies sind die Mitschriften der Mittwochübung (16:00 Uhr) in der
Prozeduralen Programmierung an der TU Bergakademie Freiberg.

Alle Lösungen sind ohne Garantie auf Korrektheit und ledeglich für
den privaten Gebrauch bestimmt.

## Buildme

Bauen aller Lösungen auf einmal mittels:
```console
$ make
```

