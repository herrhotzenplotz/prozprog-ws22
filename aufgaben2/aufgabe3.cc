#include <iostream>
#include <cmath> // Für std::pow(x, y)

int main()
{
	double const pi = 3.14159265; // Konstanter Fließkommawert
	double surface, volume, radius; // Variablen (Kommazahlen)

	// Radius abfragen
	std::cout << "Bitte den Radius eingeben: ";
	std::cin >> radius;

	// Oberfläche berechnen
	surface = 4.0 * pi * std::pow(radius, 2.0);

	// Volumen berechnen
	volume = 4.0 / 3.0 * pi * std::pow(radius, 3.0);

	// Ausgabe
	std::cout << "Radius = " << radius << std::endl
	          << "Oberfläche = " << surface << std::endl
	          << "Volumen = " << volume << std::endl;

	return 0;
}
