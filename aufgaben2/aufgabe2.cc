#include <iostream>

int main()
{
	// Vier Variablen deklarieren
	int zahl1, zahl2, summe, differenz;

	// Erste Zahl einlesen
	std::cout << "Bitte Zahl 1 eingeben: ";
	std::cin >> zahl1;

	// Zweite Zahl einlesen
	std::cout << "Bitte Zahl 2 eingeben: ";
	std::cin >> zahl2;

	// Summe und Differenz berechnen
	summe = zahl1 + zahl2;
	differenz = zahl1 - zahl2;

	// Ausgabe der Ergebnisse
	std::cout << "Du hast " << zahl1
	          << " und " << zahl2
	          << " eingegeben." << std::endl;

	std::cout << "Ergebnisse: " << std::endl
	          << "  Summe = " << summe << std::endl
	          << "  Differenz = " << differenz << std::endl;

	return 0;
}
