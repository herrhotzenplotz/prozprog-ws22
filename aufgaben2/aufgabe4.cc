#include <iostream>

int main()
{
	double const c_ms = 299792458;

	double c_kms = c_ms / 1000.0;
	double c_cms = c_ms * 100.0;

	std::cout << "Lichtgeschwindigkeit in m/s ist " << c_ms << std::endl
	          << "Lichtgeschwindigkeit in km/s ist " << c_kms << std::endl
	          << "Lichtgeschwindigkeit in cm/s ist " << c_cms << std::endl;

	return 0;
}
