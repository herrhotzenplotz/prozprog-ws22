#include <iostream>

int main(void)
{
	int a=1, b=2, c=3;
	double y=4.0,r;

	// hier den Ausdruck einfuegen

	r = ((double)(a + b - 2 * c) / y) + (double)c;
	std::cout << "Ergebnis: " << r << std::endl;

	r = ((double)(a * b + 2 * c) / y) * (double)c;
	std::cout << "Ergebnis: " << r << std::endl;

	r = ((double)(a + b) / ((double)c - y))
		- ((double)a / (double)b);
	std::cout << "Ergebnis: " << r << std::endl;

	return 0;
}
