#include <iostream>

int main()
{
	double operand1, operand2, result;
	char op;

	/* Ausdruck einlesen */
	std::cin >> operand1 >> op >> operand2;

	switch (op) {
	case '+':
		result = operand1 + operand2;
		break;
	case '-':
		result = operand1 - operand2;
		break;
	case '*':
		result = operand1 * operand2;
		break;
	case ':':
	case '/':
		if (operand2 == 0) {
			std::cerr << "Division durch Null!" << std::endl;
			return 0;
		}
		result = operand1 / operand2;
		break;
	default:
		std::cerr << "Ungültiger Operator!" << std::endl;
		return 0;
	}

	std::cout << " = " << result << std::endl;

	return 0;
}
