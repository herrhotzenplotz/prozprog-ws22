#include <iostream>

int main()
{
	double number1, number2, number3, maximum;

	std::cout << "Bitte Zahl 1 eingeben: ";
	std::cin >> number1;

	std::cout << "Bitte Zahl 2 eingeben: ";
	std::cin >> number2;

	std::cout << "Bitte Zahl 3 eingeben: ";
	std::cin >> number3;

	if (number1 > number2) {

		if (number1 > number3)
			maximum = number1;
		else
			maximum = number3;

	} else if (number1 < number2) {

		if (number2 < number3)
			maximum = number3;
		else
			maximum = number2;

	} else { /* number1 == number2 */

		if (number1 < number3)
			maximum = number3;
		else if (number1 > number3)
			maximum = number1;
		else {
			std::cout << "Alle Zahlen sind gleich" << std::endl;
			return 0;
		}
	}

	std::cout << "Das Maximum ist "
	          << maximum << std::endl;

	return 0;
}
