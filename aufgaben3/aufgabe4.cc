#include <iostream>
#include <limits.h>

int main()
{
	int max = -INT_MAX; /* ==> entspricht -unendlich */
	int n = 0;

	std::cout << "Bitte Anzahl der Zahlen eingeben: ";
	std::cin >> n;

	for (int i = 0; i < n; i++) {
		int x;

		std::cout << "Bitte Zahl " << i + 1 << " eingeben: ";
		std::cin >> x;

		if (x > max)
			max = x;
	}

	std::cout << "Das Maximum ist " << max << std::endl;

	return 0;
}
