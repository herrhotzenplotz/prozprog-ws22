#include <iostream>
#include <cmath> /* für std::fabs */

/* Taylor-Approximation der Exponentialfunktion */
int main()
{
	double g = 1;
	double x, epsilon, result = 0;
	int i = 1;

	std::cout << "Bitte x eingeben: ";
	std::cin >> x;

	std::cout << "Bitte Epsilon eingeben: ";
	std::cin >> epsilon;

	/* Endlosschleife */
	for (;;) {
		double next_g = g * (x / (double)(i));
		result += g;

		if (std::fabs(next_g) < epsilon)
			break;

		g = next_g;
		i++;
	}

	std::cout << "exp(" << x << ") = " << result << std::endl;

	return 0;
}
