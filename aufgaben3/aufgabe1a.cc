#include <iostream>

int main()
{
	double number1, number2, maximum;

	std::cout << "Bitte Zahl 1 eingeben: ";
	std::cin >> number1;

	std::cout << "Bitte Zahl 2 eingeben: ";
	std::cin >> number2;

	if (number1 > number2) {
		maximum = number1;
	} else {
		maximum = number2;
	}

	std::cout << "Das Maximum ist "
	          << maximum << std::endl;

	return 0;
}
